from collections import Counter
import time


# at least 5%
def mostActive(customers):
    s = set(cust)
    ls = []
    for x in s:
        cnt = customers.count(x)
        p = cnt * 100 / len(customers)
        if p >= 5:
            ls.append(x)
    ls.sort()
    return ls


def mostActive2(customers):
    counter = Counter(customers)
    ls = []
    for x in counter:
        p = counter[x] * 100 / len(customers)
        if p >= 5:
            ls.append(x)
    ls.sort()
    return ls


if __name__ == "__main__":
    # cust = "Omega Alpha Omega Alpha Omega Alpha Omega Alpha Omega\
    #        Alpha Omega Alpha Omega Alpha Omega Alpha Omega\
    #        Alpha Omega Beta"
    # cust = cust.split()

    cust = ["Cust"+str(x) for x in range(100000)]

    start = time.time()
    res = mostActive2(cust)
    end = time.time()
    print(end-start)
    print(res)
