from itertools import combinations


def renameFile(newName, oldName):
    comb = list(combinations(oldName, 3))
    cnt = comb.count(tuple(newName))

    return cnt % (10**9+7)


if __name__ == "__main__":
    res = renameFile("aba", "ababa")
    print(res)

    res = renameFile("ccc", "cccc")
    print(res)
