import unittest
from unittest.mock import patch
from arrays.c_new_year_chaos import minimumBribes


class Test(unittest.TestCase):

    @patch('builtins.print')
    def test_minimumBribes_0(self, mock_print):
        minimumBribes([2, 1, 5, 3, 4])
        mock_print.assert_called_with(3)

    @patch('builtins.print')
    def test_minimumBribes_1(self, mock_print):
        minimumBribes([2, 5, 1, 3, 4])
        mock_print.assert_called_with("Too chaotic")
