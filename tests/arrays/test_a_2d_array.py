import unittest
from arrays.a_2d_array import hourglassSum


class Test(unittest.TestCase):

    def test_hourglassSum_0(self):
        matrix = [[1, 1, 1, 0, 0, 0],
                  [0, 1, 0, 0, 0, 0],
                  [1, 1, 1, 0, 0, 0],
                  [0, 0, 2, 4, 4, 0],
                  [0, 0, 0, 2, 0, 0],
                  [0, 0, 1, 2, 4, 0]]
        res = hourglassSum(matrix)
        self.assertEqual(res, 19)

    def test_hourglassSum_1(self):
        matrix = [[1, 1,  1,  0,  0, 0],
                  [0, 1,  0,  0,  0, 0],
                  [1, 1,  1,  0,  0, 0],
                  [0, 9,  2, -4, -4, 0],
                  [0, 0,  0, -2,  0, 0],
                  [0, 0, -1, -2, -4, 0]]
        res = hourglassSum(matrix)
        self.assertEqual(res, 13)

    def test_hourglassSum_2(self):
        matrix = [[-9, -9, -9, 1, 1, 1],
                  [0, -9,  0, 4, 3, 2],
                  [-9, -9, -9, 1, 2, 3],
                  [0, 0, 8, 6, 6, 0],
                  [0, 0, 0, -2, 0, 0],
                  [0, 0, 1, 2, 4, 0]]
        res = hourglassSum(matrix)
        self.assertEqual(res, 28)
