import unittest
from arrays.b_left_rotation import rotLeft


class Test(unittest.TestCase):

    def test_rotLeft_0(self):
        res = rotLeft([1, 2, 3, 4, 5], 4)
        self.assertEqual(res, [5, 1, 2, 3, 4])

    def test_rotLeft_1(self):
        res = rotLeft([41, 73, 89, 7, 10, 1, 59, 58, 84, 77,
                       77, 97, 58, 1, 86, 58, 26, 10, 86, 51], 10)
        self.assertEqual(res, [77, 97, 58, 1, 86, 58, 26, 10, 86,
                               51, 41, 73, 89, 7, 10, 1, 59, 58, 84, 77])

    def test_rotLeft_2(self):
        res = rotLeft([33, 47, 70, 37, 8, 53, 13, 93, 71,
                       72, 51, 100, 60, 87, 97], 13)
        self.assertEqual(res, [87, 97, 33, 47, 70, 37, 8,
                               53, 13, 93, 71, 72, 51, 100, 60])
