import unittest
from arrays.d_swaps import minimumSwaps


class Test(unittest.TestCase):

    def test_minimumSwaps_0(self):
        res = minimumSwaps([4, 3, 1, 2])
        self.assertEqual(res, 3)

    def test_minimumSwaps_1(self):
        res = minimumSwaps([2, 3, 4, 1, 5])
        self.assertEqual(res, 3)

    def test_minimumSwaps_2(self):
        res = minimumSwaps([1, 3, 5, 2, 4, 6, 7])
        self.assertEqual(res, 3)
