import unittest
from arrays.e_array_manipulation import arrayManipulation


class Test(unittest.TestCase):

    def test_arrayManipulation_0(self):
        matrix = [[1, 2, 100],
                  [2, 5, 100],
                  [3, 4, 100]]
        res = arrayManipulation(5, matrix)
        self.assertEqual(res, 200)

    def test_arrayManipulation_1(self):
        matrix = [[x for x in range(1, 4)]]
        res = arrayManipulation(5, matrix)
        self.assertEqual(res, 3)

    def test_arrayManipulation_2(self):
        matrix = []
        n = 10000
        for x in range(n):
            matrix.append([x for x in range(1, 4)])

        res = arrayManipulation(n, matrix)
        self.assertEqual(res, 30000)

    # takes 8sec !
    def test_arrayManipulation_3(self):
        matrix = []
        n = 10000
        for x in range(n):
            matrix.append([1, n, 100])
        res = arrayManipulation(n, matrix)
        self.assertEqual(res, 1000000)
