import unittest
from warmup.d_repeated_string import repeatedString


class Test(unittest.TestCase):

    def test_repeatedString_0(self):
        res = repeatedString("aba", 10)
        self.assertEqual(7, res)

    def test_repeatedString_1(self):
        res = repeatedString("a", 1000000000000)
        self.assertEqual(1000000000000, res)
