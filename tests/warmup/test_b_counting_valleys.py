import unittest
from warmup.b_counting_valleys import countingValleys


class Test(unittest.TestCase):

    def test_coutingValleys_0(self):
        res = countingValleys(8, "UDDDUDUU")
        self.assertEqual(res, 1)

    def test_coutingValleys_1(self):
        res = countingValleys(12, "DDUUDDUDUUUD")
        self.assertEqual(res, 2)
