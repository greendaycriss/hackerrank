import unittest
from warmup.a_sales_by_match import sockMerchant


class Test(unittest.TestCase):

    def test_sockMerchant_0(self):
        res = sockMerchant(9, [10, 20, 20, 10, 10, 30, 50, 10, 20])
        self.assertEqual(res, 3)

    def test_sockMerchant_1(self):
        res = sockMerchant(10, [1, 1, 3, 1, 2, 1, 3, 3, 3, 3])
        self.assertEqual(res, 4)
