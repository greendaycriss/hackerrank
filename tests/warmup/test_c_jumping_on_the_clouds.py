import unittest
from warmup.c_jumping_on_the_clouds import jumpingOnClouds


class Test(unittest.TestCase):

    def test_jumpingOnClouds_0(self):
        res = jumpingOnClouds([0, 0, 1, 0, 0, 1, 0])
        self.assertEqual(4, res)

    def test_jumpingOnClouds_1(self):
        res = jumpingOnClouds([0, 0, 0, 1, 0, 0])
        self.assertEqual(3, res)
