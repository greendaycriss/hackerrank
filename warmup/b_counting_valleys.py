import os


# An avid hiker keeps meticulous records of their hikes.
# During the last hike that took exactly steps,
# for every step it was noted if it was an uphill, U, or a downhill, D step.
# Hikes always start and end at sea level, and each step up or down represents
# a  unit change in altitude. We define the following terms:

# A mountain is a sequence of consecutive steps above sea level,
# starting with a step up from sea level and ending with
# a step down to sea level.
# A valley is a sequence of consecutive steps
# below sea level,
# starting with a step down from sea level and ending with
# a step up to sea level.
# Given the sequence of up and down steps during a hike,
# find and print the number of valleys walked through.

# Sample Input
# 8

# UDDDUDUU

# Sample Output
# 1


# The first line contains an integer steps, the number of steps in the hike.
# The second line contains a single string path of steps
# characters that describe the path.
def countingValleys(steps, path):
    level = 0
    valleys = 0
    for x in range(steps):
        if path[x] == 'U':
            level = level - 1
            if x >= 1:
                if level == 0:
                    valleys = valleys + 1
        else:
            level = level + 1
    return valleys


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    steps = int(input().strip())

    path = input()

    result = countingValleys(steps, path)

    fptr.write(str(result) + '\n')

    fptr.close()
