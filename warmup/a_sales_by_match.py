import os

# There is a large pile of socks that must be paired by color.
# Given an array of integers representing the color of each sock,
# determine how many pairs of socks with matching colors there are.

# Sample Input
# STDIN                       Function
# -----                       --------
# 9                           n = 9
# 10 20 20 10 10 30 50 10 20  ar = [10, 20, 20, 10, 10, 30, 50, 10, 20]

# Sample Outpu
# 3


# The first line contains an integer n, the number of socks represented in .
# The second line contains n space-separated integers, ar[i],
# the colors of the socks in the pile.
def sockMerchant(n, ar):
    ar.sort()
    x = 0
    count = 0
    while x < n-1:
        if ar[x] == ar[x+1]:
            count = count + 1
            x = x + 1
        x = x + 1
    return count

# def test_sockMerchant():
    # assert sockMerchant(9, [10, 20, 20, 10, 10, 30, 50, 10, 20]) == 3


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())
    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    fptr.write(str(result) + '\n')
    fptr.close()
