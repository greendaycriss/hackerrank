import os


#
# Complete the 'jumpingOnClouds' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY c as parameter.
#


def jumpingOnClouds(c):
    path = []
    x = 0
    while x < len(c):
        if c[x] == 0:
            path.append(x)
            if x < len(c)-2 and c[x+1] == 0 and c[x+2] == 0:
                x = x + 1
        x = x + 1
    return len(path)-1


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c)

    fptr.write(str(result) + '\n')

    fptr.close()
