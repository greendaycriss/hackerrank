import os
import time

#
# Complete the 'repeatedString' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts following parameters:
#  1. STRING s
#  2. LONG_INTEGER n
#


# logical incorrect !
def repeatedString1(s, n):
    if n > len(s):
        while len(s) < n:
            s = s + s[0:n-len(s)]
    return s.count('a')


# for 10 mil takes 4.94 sec
def repeatedString2(s, n):
    stotal = s
    if n > len(s):
        while len(stotal) < n:
            for x in s:
                if len(stotal) < n:
                    stotal = stotal + x
    print(stotal)
    return stotal.count('a')


# for 1 mil problematic !
def repeatedString3(s, n):
    stotal = s
    if n > len(s):
        while len(stotal) < n:
            for x in s:
                if len(stotal) < n:
                    stotal = '%s%s' % (stotal, x)
    return stotal.count('a')


# for 10   mil takes 1. 98 sec
# for 100  mil takes 20.21 sec
# for 1000 mil problematic !
def repeatedString4(s, n):
    stotal = []

    while len(stotal) < n:
        for x in s:
            if len(stotal) < n:
                stotal.append(x)

    # stotal = ''.join(stotal)
    return stotal.count('a')


# for 1000 bil takes 0.0 sec
def repeatedString(s, n):
    cnt = s.count('a')
    cnt = cnt * (n//len(s))
    mod = n % len(s)
    if mod != 0:
        cnt = cnt + s[0:mod].count('a')
    return cnt


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    n = int(input().strip())

    start = time.time()
    result = repeatedString(s, n)
    end = time.time()
    print(end-start)

    fptr.write(str(result) + '\n')

    fptr.close()
