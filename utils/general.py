import time


def period_decorator(funct):
    def wrapper(*args, **kwargs):
        start = time.time()

        res = funct(*args, **kwargs)

        end = time.time()
        print("Function took: %.3f sec" % (end-start))

        return res
    return wrapper


@period_decorator
def my_funct(arg):
    time.sleep(2)
