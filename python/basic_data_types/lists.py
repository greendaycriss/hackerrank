# Consider a list (list = []). You can perform the following commands:

# insert i e: Insert integer  at position .
# print: Print the list.
# remove e: Delete the first occurrence of integer .
# append e: Insert integer  at the end of the list.
# sort: Sort the list.
# pop: Pop the last element from the list.
# reverse: Reverse the list.

# Initialize your list and read in the value of  followed by  lines of
# commands where each command will be of the  types listed above.
# Iterate through each command in order and perform the corresponding
# operation on your list.


def process(cmds):

    for cmd in cmds:
        if cmd == "print":
            print(a_list)

        elif cmd == "sort":
            a_list.sort()

        elif cmd == "pop":
            a_list.pop()

        elif cmd == "reverse":
            a_list.reverse()

        elif cmd.startswith("append"):
            a_list.append(int(cmd.split()[1]))

        elif cmd.startswith("insert"):
            a_list.insert(int(cmd.split()[1]), int(cmd.split()[2]))

        elif cmd.startswith("remove"):
            a_list.remove(int(cmd.split()[1]))


if __name__ == '__main__':
    N = int(input())

    a_list = []
    cmd_list = []
    for x in range(N):
        cmd_list.append(input())

    process(cmd_list)
