# Given the names and grades for each student in a class of  students,
# store them in a nested list and print the name(s) of any
# student(s) having the second lowest grade.
# Note: If there are multiple students with the second lowest grade,
# order their names alphabetically and print each name on a new line.


if __name__ == '__main__':
    students = []
    for _ in range(int(input())):
        name = input()
        score = float(input())
        students.append([name, score])
    # print(students)

    # sort asc by grade
    for x in range(len(students)-1):
        swap = False
        for x in range(len(students)-1):
            if students[x][1] > students[x+1][1]:
                students[x], students[x+1] = students[x+1], students[x]
                swap = True
        if swap is False:
            break
    # print(students)

    second_min_grade = students[0][1]
    names = []
    found = False
    for x in range(len(students)):
        if found is True:
            if students[x][1] == second_min_grade:
                names.append(students[x][0])
        else:
            if students[x][1] > second_min_grade:
                names.append(students[x][0])
                second_min_grade = students[x][1]
                found = True

    # sort asc alphanum
    for x in range(len(names)-1):
        swap = False
        for x in range(len(names)-1):
            if names[x][0] > names[x+1][0]:
                names[x], names[x+1] = names[x+1], names[x]
                swap = True
        if swap is False:
            break

    for name in names:
        print(name)
