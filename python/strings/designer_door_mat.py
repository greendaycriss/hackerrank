
if __name__ == "__main__":
    N = 7
    M = 21

    s = ".|."

    for x in range(N//2):
        new_s = (2*x+1) * s
        print(new_s.center(M, "-"))

    print("WELCOME".center(M, "-"))

    for x in reversed(range(N//2)):
        new_s = (2*x+1) * s
        print(new_s.center(M, "-"))
