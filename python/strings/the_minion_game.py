def count(s1, s2):
    cnt = 0

    for x in range(len(s2)):
        k = 0
        found = False
        while k < len(s1) and (x + k) < len(s2):

            if s1[k] != s2[x + k]:
                found = False
                break
            else:
                found = True
                k += 1
        if found is True and k == len(s1):
            cnt += 1

    return cnt


def minion_game(string):
    vowels = ['A', 'E', 'I', 'O', 'U']

    # Stuart
    cnt = 0
    for x in range(len(string)):
        if string[x] not in vowels:
            if string[:x].find(string[x]) == -1:
                cnt += string.count(string[x])

            y = x + 1
            w = string[x]
            while y < len(string):
                w += string[y]
                if string[:y].find(w) == -1:
                    cnt += count(w, string)
                y += 1

    # Kevin
    cnt2 = 0
    for x in range(len(string)):
        if string[x] in vowels:
            if string[:x].find(string[x]) == -1:
                cnt2 += string.count(string[x])
            y = x + 1
            w = string[x]
            while y < len(string):
                w += string[y]
                if string[:y].find(w) == -1:
                    cnt2 += count(w, string)
                y += 1

    if cnt > cnt2:
        print("Stuart " + str(cnt))
    elif cnt < cnt2:
        print("Kevin " + str(cnt2))
    else:
        print("Draw")


if __name__ == '__main__':
    s = input()
    minion_game(s)
