# Exceptions
# Errors detected during execution are called exceptions.


if __name__ == '__main__':
    N = int(input())

    for x in range(N):
        try:
            vals = input().split()
            print(int(vals[0])//int(vals[1]))
        except Exception as e:
            print("Error Code:", e)
