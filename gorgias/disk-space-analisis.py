import os
# import string


def segments(x, space):
    space_range = []
    for i in range(space_count-x+1):
        space_range.append(space[i:i+x])

    minimum = []
    for i in space_range:
        minimum.append(min(i))

    return max(minimum)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # x = int(input().strip())
    x = 3

    # space_count = int(input().strip())
    space_count = 5

    space = []
    # for _ in range(space_count):
    # space_item = int(input().strip())
    # space.append(space_item)

    # space = [1, 2, 3, 1, 2]
    # space = [8, 2, 4, 6]
    space = [2, 5, 4, 6, 8]

    result = segments(x, space)

    fptr.write(str(result) + '\n')
    fptr.close()
