import os
import string


def slowestKey(keyTimes):
    key = keyTimes[0][1]
    time = keyTimes[1][1]
    for x in range(1, len(keyTimes)):
        temp = keyTimes[x][1] - keyTimes[x-1][1]
        if temp > time:
            time = temp
            key = keyTimes[x][0]
    letters = string.ascii_lowercase
    return letters[key]


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    # keyTimes_rows = int(input().strip())
    # keyTimes_columns = int(input().strip())

    keyTimes = []
    # for _ in range(keyTimes_rows):
    # keyTimes.append(list(map(int, input().rstrip().split())))

    keyTimes = [[0, 2], [1, 5], [0, 9], [2, 15]]

    result = slowestKey(keyTimes)

    fptr.write(str(result) + '\n')
    fptr.close()
