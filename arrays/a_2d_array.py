import os

# Sample input:

# 1 1 1 0 0 0
# 0 1 0 0 0 0
# 1 1 1 0 0 0
# 0 0 2 4 4 0
# 0 0 0 2 0 0
# 0 0 1 2 4 0

# Out: 19


def sum(arr, n1, n2, m1, m2):
    s = 0
    cnt = 0
    y = 0
    for x in range(n1, n2):
        cnt = cnt + 1
        if cnt == 2:
            s = s + arr[x][y-1]
            continue

        for y in range(m1, m2):
            s = s + arr[x][y]
    return s


def hourglassSum(arr):
    sums = []
    for x in range(4):
        for y in range(4):
            s = sum(arr, x, x+3, y, y+3)
            sums.append(s)
    sums.sort(reverse=True)
    return sums[0]


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    arr = []

    for _ in range(6):
        arr.append(list(map(int, input().rstrip().split())))

    result = hourglassSum(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
