import os


# You are given an unordered array consisting of
# consecutive integers  [1, 2, 3, ..., n] without any duplicates.
# You are allowed to swap any two elements.
# Find the minimum number of swaps required to sort the array
# in ascending order.

# Example


# Perform the following steps:

# i   arr = [7, 1, 3, 2, 4, 5, 6]  swap (indices)
# 0   [7, 1, 3, 2, 4, 5, 6]       swap (0,3)
# 1   [2, 1, 3, 7, 4, 5, 6]       swap (0,1)
# 2   [1, 2, 3, 7, 4, 5, 6]       swap (3,4)
# 3   [1, 2, 3, 4, 7, 5, 6]       swap (4,5)
# 4   [1, 2, 3, 4, 5, 7, 6]       swap (5,6)
# 5   [1, 2, 3, 4, 5, 6, 7]


# Complete the minimumSwaps function below.
# Insertion sort - dosen't fit the purpose
def minimumSwaps1(arr):
    swap = 0
    for x in range(1, len(arr)):
        key = arr[x]
        y = x - 1
        while y > 0 and key < arr[y-1]:
            arr[y] = arr[y-1]
            y = y - 1
            swap += 1
        arr[y] = key
    return swap


# Takes too much time: 27.45 sec !
# logic to fetch the right element to the current pos
# problem: you have to find the index
def minimumSwaps2(arr):
    swap = 0
    for x in range(len(arr)-1):
        if arr[x] != x+1:
            index = arr.index(x+1, x, len(arr)-x)
            arr[x], arr[index] = arr[index], arr[x]
            swap += 1
    return arr


# improvment per #geeksforgeeks.org
# switch the logic and move the current element
# where it belongs (already known since el are consecutive)
def minimumSwaps(arr):
    swap_cnt = 0
    for _ in range(len(arr)-1):
        swapped = False
        for x in range(len(arr)-1):
            if arr[x] != x+1:
                temp = arr[arr[x]-1]
                arr[arr[x]-1] = arr[x]
                arr[x] = temp
                swapped = True
                swap_cnt += 1
        if swapped is False:
            break

    return swap_cnt


# with my search
# Takes too much time: 151.79 sec !
def minimumSwaps3(arr):
    swap = 0
    for x in range(len(arr)-1):
        if arr[x] != x+1:
            index = -1
            for y in range(x, len(arr)):
                if arr[y] == x+1:
                    index = y
                    break
            arr[x], arr[index] = arr[index], arr[x]
            swap += 1
        # print(arr)
    return arr


# Quick sort
# Isnt' built to work with min num of swaps
def partition(start, end, array):
    pivot_index = start
    pivot = array[pivot_index]
    global swaps
    while start < end:
        while start < len(array) and array[start] <= pivot:
            start += 1

        while array[end] > pivot:
            end -= 1

        if(start < end):
            array[start], array[end] = array[end], array[start]

    array[end], array[pivot_index] = array[pivot_index], array[end]
    swaps += 1
    return end


def quick_sort(start, end, array):
    if (start < end):
        p = partition(start, end, array)
        quick_sort(start, p - 1, array)
        quick_sort(p + 1, end, array)
    return swaps


def minimumSwaps4(arr):
    quick_sort(0, len(arr) - 1, arr)
    return swaps


# selection sort #my version(stable)
# takes 61.036
def minimumSwaps5(arr):
    swaps = 0
    for x in range(len(arr)):
        m = min(arr[x:])
        index = arr.index(m, x)
        if index != x:
            arr[x], arr[index] = arr[index], arr[x]
            swaps += 1
    return swaps


# selection sort #geeksforgeeks version
# takes 236.75 sec !
def minimumSwaps6(arr):
    for i in range(len(arr)):
        min_idx = i
        for j in range(i+1, len(arr)):
            if arr[min_idx] > arr[j]:
                min_idx = j
        arr[i], arr[min_idx] = arr[min_idx], arr[i]


# calc num of not in place elements
def minimumSwaps7(arr):
    cnt = 0
    for x in range(len(arr)-1):
        if arr[x] != x+1:
            cnt += 1
    print(cnt)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    swaps = 0
    arr = list(map(int, input().rstrip().split()))

    res = minimumSwaps(arr)

    fptr.write(str(res) + '\n')

    fptr.close()
