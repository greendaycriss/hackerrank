# Complete the 'minimumBribes' function below.
#
# The function accepts INTEGER_ARRAY q as parameter.


# for 10 000 elements takes: 09.67 sec
def minimumBribes1(q):
    for y in range(n-1):
        for x in range(n-1):
            if q[x] > q[x+1]:
                temp = q[x]
                q[x] = q[x+1]
                q[x+1] = temp


# for 10 000 elements takes: 09.55 sec
# multiple assigment
def minimumBribes2(q):
    for y in range(n-1):
        for x in range(n-1):
            if q[x] > q[x+1]:
                q[x], q[x+1] = q[x+1], q[x]


# for 10 000 elements takes: 6.37 sec
# reduce number of elments per loop !
def minimumBribes3(q):
    for y in range(n-1):
        for x in range(n-y-1):
            if q[x] > q[x+1]:
                q[x], q[x+1] = q[x+1], q[x]


# for 10 000 elements takes: 6.18 sec
# stop alg if no swap was made in one loop !
def minimumBribes4(q):
    for y in range(n-1):
        swapped = False
        for x in range(n-y-1):
            if q[x] > q[x+1]:
                q[x], q[x+1] = q[x+1], q[x]
                swapped = True
        if swapped is False:
            break


# for 10 000 elements takes: 6.37 sec
# wrong implementation
def minimumBribes(q):
    consec = 0
    cnt = 0
    for y in range(len(q)-1):
        swapped = False
        for x in range(len(q)-y-1):
            if q[x] > q[x+1]:
                q[x], q[x+1] = q[x+1], q[x]
                consec = consec + 1
                cnt = cnt + 1
                swapped = True
            else:
                consec = 0
            if consec > 2:
                print("Too chaotic")
                return
        consec = 0
        if swapped is False:
            break
    print(cnt)


if __name__ == '__main__':
    t = int(input().strip())

    for t_itr in range(t):
        n = int(input().strip())

        q = list(map(int, input().rstrip().split()))

        minimumBribes(q)
