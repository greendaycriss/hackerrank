import os


# Complete the 'rotLeft' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER d


# for 10  thausand taks 6.12 sec
# for 100 thausand taks too much !
def rotLeft1(a, d):
    for y in range(d):
        temp = a[0]
        for x in range(n-1):
            a[x] = a[x+1]
        a[n-1] = temp
    return a


# for 10  thausand taks 0.16 sec
# for 100 thausand taks 1.87 sec
def rotLeft(a, d):
    for x in range(d):
        first = a.pop(0)
        a.append(first)
    return a


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    d = int(first_multiple_input[1])

    a = list(map(int, input().rstrip().split()))

    result = rotLeft(a, d)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
