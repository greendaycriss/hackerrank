import os


# Starting with a 1-indexed array of zeros and a list of operations,
# for each operation add a value to each the array element
# between two given indices, inclusive.
# Once all operations have been performed,
# return the maximum value in the array.

# Example
# n = 10
# quries=[[1,5,3], [4,8,7], [6,9,1]]
# Queries are interpreted as follows:

#     a b k
#     1 5 3
#     4 8 7
#     6 9 1


#
# Complete the 'arrayManipulation' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. 2D_INTEGER_ARRAY queries
#


# takes 8.054s
def arrayManipulation2(n, queries):
    # print(queries)
    array = [0 for x in range(n)]
    for x in range(len(queries)):
        for y in range(queries[x][0]-1, queries[x][1]):
            array[y] += queries[x][2]
    return max(array)


# takes 7.615s
def arrayManipulation(n, queries):
    array = [0 for x in range(n)]
    for x in range(len(queries)):
        interval = queries[x][1] - queries[x][0]
        interval += 1
        array[queries[x][0]-1:queries[x][1]] = \
            [array[k]+queries[x][2] for k in
                range(queries[x][0]-1, queries[x][1])]
    return max(array)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    queries = []

    for _ in range(m):
        queries.append(list(map(int, input().rstrip().split())))

    result = arrayManipulation(n, queries)

    fptr.write(str(result) + '\n')

    fptr.close()
