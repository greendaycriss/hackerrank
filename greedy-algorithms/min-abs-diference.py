import os
import itertools
import time

#
# Complete the 'minimumAbsoluteDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#


# for array of 10 000 elments takes 8.80 sec
def minimumAbsoluteDifference1(arr):
    seq = itertools.combinations(arr, 2)
    tuples = list(seq)
    min = pow(10, 9)
    for x in tuples:
        diff = abs(x[0]-x[1])
        if diff < min:
            min = diff
    return min


def absDiff(elem):
    return abs(elem[0]-elem[1])


# for array of 10 000 elments takes 5.96 sec
def minimumAbsoluteDifference2(arr):
    seq = itertools.combinations(arr, 2)

    start1 = time.time()
    m = min(seq, key=absDiff)
    end1 = time.time()
    print(end1-start1)

    return absDiff(m)


# for array of 10 000 elments takes 6.96 sec
def minimumAbsoluteDifference3(arr):
    seq = itertools.combinations(arr, 2)
    test = [abs(tup[0]-tup[1]) for tup in seq]
    return min(test)


# for array of 10 000 elments takes 0.01 sec
def minimumAbsoluteDifference(arr):
    sorted_arr = sorted(arr)
    min_val = sorted_arr[-1] - sorted_arr[0]
    for i, j in zip(sorted_arr[:-1], sorted_arr[1:]):
        min_val = min(min_val, j - i)
    return min_val


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # n = int(input().strip())

    # arr = list(map(int, input().rstrip().split()))
    arr = [x for x in range(10000)]
    # print(arr)

    start = time.time()
    result = minimumAbsoluteDifference(arr)
    end = time.time()
    print("res =", result)
    print("time =", end-start)

    fptr.write(str(result) + '\n')

    fptr.close()
